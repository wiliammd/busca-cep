import axios from 'axios';
import React, { useState } from 'react';
import { toast } from 'react-toastify';
import './App.css';
const BASE_URL = 'https://viacep.com.br/ws'
type Adress = {
  logradouro: string;
  localidade: string;
}
function App() {
  const [searchValue, setSearchValue] = useState('');
  const [addressData, setAddressData] = useState<Adress>();

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setAddressData(undefined);
    axios(`${BASE_URL}/${searchValue}/json`)
      .then(response => setAddressData(response.data))
      .catch(() => toast.error("Error Notification !", {
        position: toast.POSITION.TOP_LEFT
      }))
  }
  return (
    <div className="container-wrapper">
      <div className="container">
        <h1 className="title">Busca CEP</h1>
        <form className="search-form" onSubmit={handleSubmit}>
          <input type="text" maxLength={9} className="search-input" 
          placeholder="CEP SOMENTE NUMEROS" value={searchValue.replace('-','')} 
          onChange={event => setSearchValue(event.target.value)} />
          <button className="search-button">Buscar</button>
          {addressData && (
            <>
              <div className="serach-result-item">
                <strong className="result-title">Logradouro</strong><br />
                <span className="result-subtitle">{addressData.logradouro !== '' ? addressData.logradouro :"Não encontrado!" }</span>
              </div>
              <div className="serach-result-item">
                <strong className="result-title">Localidade</strong><br />
                <span className="result-subtitle">{addressData.localidade}</span>
              </div>
            </>
          )}
        </form>
      </div>
    </div>
  );
}

export default App;
